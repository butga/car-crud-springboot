package id.co.nexsoft.carcrudspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCrudSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarCrudSpringbootApplication.class, args);
	}

}
