package id.co.nexsoft.carcrudspringboot.response;

import org.springframework.stereotype.Component;

import java.net.PortUnreachableException;

@Component
public class CommonResponseGenerator<T> {

    public <T> CommonResponse<T> successResponse(T datas, String message) {
        CommonResponse commonResponse = new CommonResponse<T>();
        commonResponse.setStatus("200");
        commonResponse.setMessage(message);
        commonResponse.setDatas(datas);

        return commonResponse;
    }

    public <T> CommonResponse<T> failedResponse(String message) {
        CommonResponse commonResponse = new CommonResponse<T>();
        commonResponse.setStatus(500);
        commonResponse.setMessage(message);

        return commonResponse;
    }
}
