package id.co.nexsoft.carcrudspringboot.controllers;

import id.co.nexsoft.carcrudspringboot.entities.CarEntity;
import id.co.nexsoft.carcrudspringboot.repositories.CarRepository;
import id.co.nexsoft.carcrudspringboot.response.CommonResponse;
import id.co.nexsoft.carcrudspringboot.response.CommonResponseGenerator;
import id.co.nexsoft.carcrudspringboot.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "car")
public class CarController {

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarService carService;

    @Autowired
    CommonResponseGenerator commonResponseGenerator;

    @GetMapping(value = "check-api")
    public CommonResponse<String> checkApi() {
        return commonResponseGenerator.successResponse("Heloo", "Success Check API");
    }

    @PostMapping(value = "add")
    public CommonResponse<CarEntity> add(@RequestBody CarEntity param) {

        CarEntity car = carService.addCar(param);

        return commonResponseGenerator.successResponse(car, "success add new data");
    }

    @GetMapping(value = "get")
    public CommonResponse<List<CarEntity>> get() {

        List<CarEntity> carList = carService.getAllCar();

        return commonResponseGenerator.successResponse(carList, "success get all data");
    }

    @GetMapping(value = "getById")
    public CommonResponse<CarEntity> getById(@RequestParam int id) {

        CarEntity car = carService.getCarById(id);

        return commonResponseGenerator.successResponse(car, "success get data by id");
    }

    @PostMapping(value = "update")
    public CommonResponse<CarEntity> update(@RequestBody CarEntity param) {

        CarEntity car = carService.updateCar(param);

        return commonResponseGenerator.successResponse(car, "succes update");
    }

    @PostMapping(value = "delete")
    public CommonResponse<List<CarEntity>> delete(@RequestParam int id) {

        carService.deleteCar(id);

        List<CarEntity> carList = carService.getAllCar();

        return commonResponseGenerator.successResponse(carList, "sukses update data");
    }

}
