package id.co.nexsoft.carcrudspringboot.services;

import id.co.nexsoft.carcrudspringboot.entities.CarEntity;
import id.co.nexsoft.carcrudspringboot.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CarServicesImpl implements CarService{

    @Autowired
    CarRepository carRepository;

    @Override
    public CarEntity addCar(CarEntity param) {
        return  carRepository.save(param);
    }

    @Override
    public List<CarEntity> getAllCar() {
        return carRepository.findAll();
    }

    @Override
    public CarEntity getCarById(int id) {
        return carRepository.findById(id).get();
    }

    @Override
    public CarEntity updateCar(CarEntity param) {
        return carRepository.save(param);
    }

    @Override
    public void deleteCar(int id) {
        carRepository.deleteById(id);
    }
}
