package id.co.nexsoft.carcrudspringboot.services;

import id.co.nexsoft.carcrudspringboot.entities.CarEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CarService {
    CarEntity addCar(CarEntity carEntity);

    List<CarEntity> getAllCar();

    CarEntity getCarById(int id);

    CarEntity updateCar(CarEntity param);

    void deleteCar(int id);
}
